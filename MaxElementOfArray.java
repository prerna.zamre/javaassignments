/*
 Time complexity O(n)
 Space Complexity O(1)


 
 */
 class MaxElementOfArray
{
	public static void main(String args[])
	{
		int[] a = new int[] { 2, 60, 50, 100, 4, 81};
		int max = a[0];
		for(int i = 1; i < a.length;i++)
		{
			if(a[i] > max)
			{
				max = a[i];
			}
		}
		
		System.out.println("The Given Array Element is:");
		for(int i = 0; i < a.length;i++)
		{
			System.out.println(a[i]);
		}
		
		System.out.println("Max Element of array is:" + max);
	}
}