/*1.What is the time complexity of the following code :
    int a = 0;
    for (i = 0; i < N; i++) {
        for (j = N; j > i; j--) {
            a = a + i + j;
        }
    }
Ans=O(N^2)

2. In a competition, four different functions are observed. All the functions use a single for loop and within the for loop, the same set of statements are executed.
Consider the following for loops:
  A) for(i = 0; i < n; i++)  Ans=O(N)

  B) for(i = 0; i < n; i += 2) Ans=O(N/2)

  C) for(i = 1; i < n; i *= 2)

  D) for(i = n; i > -1; i /= 2) Ans=Infinite

3.If n is the size of input(positive), which function is the most efficient? In other words, which loop completes the fastest.

 What is time complexity of following code :
        int count = 0;
        for (int i = N; i > 0; i /= 2) {
            for (int j = 0; j < i; j++) {
                count += 1;
            }
        }
Ans=O(N)
*/